<?php
use Fenix440\Model\Intensity\Traits\IntensityTrait;
use Fenix440\Model\Intensity\Interfaces\IntensityAware;

/**
 * Class IntensityTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Intensity\Traits\IntensityTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class IntensityTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Intensity\Interfaces\IntensityAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Intensity\Traits\IntensityTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     * @covers  ::setIntensity
     * @covers  ::isIntensityValid
     * @covers  ::getIntensity
     * @covers  ::getDefaultIntensity
     * @covers  ::hasDefaultIntensity
     * @covers  ::hasIntensity
     */
    public function setAndGetLowIntensity()
    {
        $trait = $this->getTraitMock();
        $intensity = \Fenix440\Model\Intensity\Interfaces\IntensityLevel::LOW_LEVEL;
        $trait->setIntensity($intensity);

        $this->assertSame($intensity,$trait->getIntensity(),'Intensity level is invalid');
    }

    /**
     * @test
     * @covers  ::setIntensity
     * @covers  ::isIntensityValid
     * @covers  ::getIntensity
     * @covers  ::getDefaultIntensity
     * @covers  ::hasDefaultIntensity
     * @covers  ::hasIntensity
     */
    public function setAndGetMediumIntensity()
    {
        $trait = $this->getTraitMock();
        $intensity = \Fenix440\Model\Intensity\Interfaces\IntensityLevel::MEDIUM_LEVEL;
        $trait->setIntensity($intensity);

        $this->assertSame($intensity,$trait->getIntensity(),'Intensity level is invalid');
    }

    /**
     * @test
     * @covers  ::setIntensity
     * @covers  ::isIntensityValid
     * @covers  ::getIntensity
     * @covers  ::getDefaultIntensity
     * @covers  ::hasDefaultIntensity
     * @covers  ::hasIntensity
     */
    public function setAndGetHighIntensity()
    {
        $trait = $this->getTraitMock();
        $intensity = \Fenix440\Model\Intensity\Interfaces\IntensityLevel::HIGH_LEVEL;
        $trait->setIntensity($intensity);

        $this->assertSame($intensity,$trait->getIntensity(),'Intensity level is invalid');
    }


    /**
     * @test
     * @covers  ::setIntensity
     * @covers  ::isIntensityValid
     * @expectedException \Fenix440\Model\Intensity\Exceptions\InvalidIntensityException
     */
    public function setInvalidIntensity()
    {
        $trait = $this->getTraitMock();
        $intensity = 12;

        $trait->setIntensity($intensity);
    }

    /**
     * @test
     * @covers  ::setIntensity
     * @covers  ::isIntensityValid
     * @expectedException \Fenix440\Model\Intensity\Exceptions\InvalidIntensityException
     */
    public function setInvalidStringIntensity()
    {
        $trait = $this->getTraitMock();
        $intensity = "low";

        $trait->setIntensity($intensity);
    }

    /**
     * @test
     * @covers  ::getIntensity
     * @covers  ::getDefaultIntensity
     * @covers  ::hasIntensity
     * @covers  ::hasDefaultIntensity
     */
    public function testDefaultIntensity()
    {
        $trait = $this->getTraitMock();
        $this->assertNull($trait->getIntensity(),'Default intensity level is invalid');
    }

}