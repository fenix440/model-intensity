<?php namespace Fenix440\Model\Intensity\Interfaces; 

/**
 * Interface IntensityLevel
 *
 * A component must know Intensity level options
 * @see IntensityAware
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Intensity\Interfaces
*/
interface IntensityLevel {

    /**
     * Low intensity level
     */
    const LOW_LEVEL = 0;

    /**
     * Medium intensity level
     */
    const MEDIUM_LEVEL = 1;

    /**
     * High intensity level
     */
    const HIGH_LEVEL = 2;

}