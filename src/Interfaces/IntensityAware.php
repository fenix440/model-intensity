<?php namespace Fenix440\Model\Intensity\Interfaces;
use Fenix440\Model\Intensity\Exceptions\InvalidIntensityException;

/**
 * Interface IntensityAware
 *
 * A component must be aware of intensity
 * @see IntensityLevel
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Intensity\Interfaces
*/
interface IntensityAware {

    /**
     * Set intensity level
     *
     * @param int $intensity Intensity level
     * @return void
     * @see IntensityLevel::LOW_LEVEL
     * @see IntensityLevel::MEDIUM_LEVEL
     * @see IntensityLevel::HIGH_LEVEL
     *
     * @throws InvalidIntensityException If intensity is invalid
     */
    public function setIntensity($intensity);

    /**
     * Get intensity level
     *
     * @return int|null
     */
    public function getIntensity();

    /**
     * Get Default intensity level
     *
     * @return int|null
     */
    public function getDefaultIntensity();

    /**
     * Checks if default intensity is set
     *
     * @return bool true/false
     */
    public function hasDefaultIntensity();

    /**
     * Checks if intensity is set
     *
     * @return bool true/false
     */
    public function hasIntensity();

    /**
     * Validates if intensity is valid
     *
     * @param mixed $intensity Intensity level
     * @return bool true/false
     */
    public function isIntensityValid($intensity);


}