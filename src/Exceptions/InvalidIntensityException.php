<?php  namespace Fenix440\Model\Intensity\Exceptions; 
/**
 * Class InvalidIntensityException
 *
 * Throws an exception if intensity is invalid
 *
 * @package Fenix440\Model\Intensity\Exceptions 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class InvalidIntensityException extends \InvalidArgumentException{

 

}

 