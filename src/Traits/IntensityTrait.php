<?php  namespace Fenix440\Model\Intensity\Traits;
use Aedart\Validate\Number\IntegerValidator;
use Fenix440\Model\Intensity\Exceptions\InvalidIntensityException;
use Fenix440\Model\Intensity\Interfaces\IntensityLevel;

/**
 * Trait IntensityTrait
 *
 * @see IntensityAware
 * @see IntensityLevel
 *
 * @package      Fenix440\Model\Intensity\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait IntensityTrait {

    /**
     * Intensity level
     *
     * @var null|int
     */
    protected $intensity=null;

    /**
     * Set intensity level
     *
     * @param int $intensity Intensity level
     * @return void
     * @see IntensityLevel::LOW_LEVEL
     * @see IntensityLevel::MEDIUM_LEVEL
     * @see IntensityLevel::HIGH_LEVEL
     *
     * @throws InvalidIntensityException If intensity is invalid
     */
    public function setIntensity($intensity){
        if(!$this->isIntensityValid($intensity))
            throw new InvalidIntensityException(sprintf('Intensity %s is invalid',$intensity));
        $this->intensity=$intensity;
    }

    /**
     * Get intensity level
     *
     * @return int|null
     */
    public function getIntensity(){
        if(!$this->hasIntensity() && $this->hasDefaultIntensity())
            $this->setIntensity($this->getDefaultIntensity());
        return $this->intensity;
    }

    /**
     * Get Default intensity level
     *
     * @return int|null
     */
    public function getDefaultIntensity(){
        return null;
    }

    /**
     * Checks if default intensity is set
     *
     * @return bool true/false
     */
    public function hasDefaultIntensity(){
        return (!is_null($this->getDefaultIntensity()))? true:false;
    }

    /**
     * Checks if intensity is set
     *
     * @return bool true/false
     */
    public function hasIntensity(){
        return (!is_null($this->intensity))? true:false;
    }

    /**
     * Validates if intensity is valid
     *
     * @param mixed $intensity Intensity level
     * @return bool true/false
     */
    public function isIntensityValid($intensity){
        $status=false;
        if(!IntegerValidator::isValid($intensity))
            return $status;
        switch($intensity){
            case IntensityLevel::LOW_LEVEL:
                $status=true;
                break;
            case IntensityLevel::MEDIUM_LEVEL:
                $status=true;
                break;
            case IntensityLevel::HIGH_LEVEL:
                $status=true;
                break;
        }
        return $status;
    }

}